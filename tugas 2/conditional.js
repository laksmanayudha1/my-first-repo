// tugas if-else
var nama = "John"
var peran = "Penyihir"


if (nama == ''){
    console.log("Nama harus diisi!")
} else if(peran == ''){
    console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!")
} else{
    if (peran.toLowerCase() == 'penyihir'){
        console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    }else if (peran.toLowerCase() == 'guard'){
        console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if (peran.toLowerCase() == 'warewolf'){
        console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!" )
    }
}


// tugas switch case
var hari = 1; 
var bulan = 4; 
var tahun = 1999;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

var namaBulan = '';

switch(bulan){
    case 1: {namaBulan = "Januari"; break;}
    case 2: {namaBulan = "Februari"; break;}
    case 3: {namaBulan = "Maret"; break;}
    case 4: {namaBulan = "April"; break;}
    case 5: {namaBulan = "Mei"; break;}
    case 6: {namaBulan = "Juni"; break;}
    case 7: {namaBulan = "Juli"; break;}
    case 8: {namaBulan = "Agustus"; break;}
    case 9: {namaBulan = "September"; break;}
    case 10: {namaBulan = "Oktober"; break;}
    case 11: {namaBulan = "November"; break;}
    case 12: {namaBulan = "Desember"; break;}
    default : {namaBulan = "(bulan tidak ditemukan)"; break;}
}
 console.log(hari + " " + namaBulan + " " + tahun)
